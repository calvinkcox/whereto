// @flow
import React from 'react'
import styled from 'styled-components'

import './App.css'
import { Row, Column } from './components/shared'
import PageHeader from './components/PageHeader'
import SearchForm from './containers/SearchForm'
import Filters from './containers/Filters'
import Results from './containers/Results'
import colors from './theme/colors'

export const Layout = styled(Column)`
  height: 100%;
  a {
    color: ${ colors.linkBlue };
    text-decoration: underline;
    cursor: pointer;
  }
`;

export const Content = styled(Row)`
  flex: 1;
  overflow-y: auto;
  overflow-x: hidden;
  padding: 26px 42px 0 32px;
`;

export default function App() {
  return (
    <Layout>
      <PageHeader/>
      <SearchForm/>
      <Content>
        <Filters/>
        <Results/>
      </Content>
    </Layout>
  );
}
