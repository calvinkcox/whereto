// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import createMockStore from './test-helpers/createMockStore'

describe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={ createMockStore() }>
        <App />
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  })
})
