// @flow
import React from 'react'

import { CheckboxLayout } from './styles'

export default function Checkbox({ id, label }: { id: string, label: string }) {
  return (
    <CheckboxLayout htmlFor={ id }>
      { label }
      <input id={ id } name={ id } type="checkbox" />
      <span />
    </CheckboxLayout>
  )
}
