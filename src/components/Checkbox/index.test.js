// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { CheckboxLayout } from './styles'
import Checkbox from './index';

describe('Checkbox', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Checkbox id="id" label="label" />, div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('sets the id and label', () => {
    const component = shallow(<Checkbox id="id" label="label"/>)

    expect(component.find(CheckboxLayout).prop('htmlFor')).toEqual('id')
    expect(component.find(CheckboxLayout).text()).toEqual('label')

    expect(component.find('input').prop('id')).toEqual('id')
    expect(component.find('input').prop('name')).toEqual('id')
  })
})
