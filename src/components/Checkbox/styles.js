import styled from 'styled-components'

import colors from '../../theme/colors'

export const CheckboxLayout = styled.label`
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  user-select: none;
  
  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }
  
  span {
    position: absolute;
    top: 0;
    left: 0;
    height: 17px;
    width: 17px;
    background-color: ${ colors.white };
    border: 1px solid ${ colors.darkGray };
  }
  
  &:hover input ~ span {
    background-color: ${ colors.darkGray };
  }
  
  span:after {
    content: "";
    position: absolute;
    display: none;
  }
  
  & input:checked ~ span:after {
    display: block;
  }
  
  & span:after {
    left: 5px;
    top: 1px;
    width: 5px;
    height: 10px;
    border: solid ${ colors.black };
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
  }
`;
