import React from 'react'

import { DualSliderLayout } from './styles'

export default function DualSlider({ id, label }: { id: string, label: string }) {
  return (
    <DualSliderLayout>
      <label htmlFor={ id }>{ label }</label>
      <input id={ id } name={ id } type="range" />
    </DualSliderLayout>
  )
}
