// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import DualSlider from './index';
import { shallow } from 'enzyme/build/index'

describe('DualSlider', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<DualSlider id="id" label="label" />, div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('sets the id and label', () => {
    const component = shallow(<DualSlider id="id" label="label"/>)

    expect(component.find('label').prop('htmlFor')).toEqual('id')
    expect(component.find('label').text()).toEqual('label')

    expect(component.find('input').prop('id')).toEqual('id')
    expect(component.find('input').prop('name')).toEqual('id')
  })
})
