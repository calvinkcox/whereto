import styled from 'styled-components'

export const DualSliderLayout = styled.div`
  margin-bottom: 17px;
  
  label {
    display: block;
    margin-bottom: 9px;
  }
`
