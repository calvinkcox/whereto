// @flow
import React from 'react'
import styled from 'styled-components'

const HR = styled.hr`
  margin: 0;
  padding-top: 6px;
  border: 0;
  border-bottom: 3px solid ${ props => props.borderColor || '' };
  ${ props => props.width ? `width: ${ props.width };` : '' }
`

export default function HeaderRule(props: { borderColor?: string, width?: string }) {
  return (
    <HR { ...props } />
  )
}
