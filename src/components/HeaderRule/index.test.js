// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import HeaderRule from '../HeaderRule'

describe('HeaderRule', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<HeaderRule />, div);
    ReactDOM.unmountComponentAtNode(div);
  })
})
