// @flow
import React from 'react'

import { Row } from '../shared'
import { Header, UserIcon } from './styles'

export default function PageHeader() {
  return (
    <Header>
      <Row>
        <UserIcon />
        Hey, James
      </Row>
    </Header>
  )
}
