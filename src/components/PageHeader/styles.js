// @flow
import styled from 'styled-components'

import colors from '../../theme/colors'
import fonts from '../../theme/fonts'

export const Header = styled.header`
  box-size: border-box;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  border: 1px solid ${ colors.lightGray };
  height: 37px;
  background-color: ${ colors.white };
  padding: 0 24px;
  font-family: ${ fonts.primary };
  font-weight: 300;
`

export const UserIcon = styled.div`
  background: url('images/avatar-icon@3x.png') top left no-repeat;
  background-size: 14px 14px;
  width: 14px;
  height: 14px;
  margin-right: 9px;
`
