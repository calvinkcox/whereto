import React from 'react'

import { RatingLayout } from './styles'

export default function Rating({ score }: { score: number }) {
  return (
    <RatingLayout>
      {
        [ 1, 2, 3, 4, 5 ].map(rank => (
          <div key={ rank } className={ [ 'star', score >= rank ? 'filled' : '' ].join(' ') } />
        ))
      }
    </RatingLayout>
  )
}
