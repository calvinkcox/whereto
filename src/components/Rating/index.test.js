// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme'
import Rating from './index';

describe('Rating', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Rating score={ 1 } />, div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('has always has 5 available stars', () => {
    const component = shallow(<Rating score={0} />);
    expect(component.find('div.star').length).toEqual(5)
  })

  it('fills in the stars based on score', () => {
    const component = shallow(<Rating score={ 2 } />);
    expect(component.find('div.star').at(0).prop('className')).toContain('filled')
    expect(component.find('div.star').at(1).prop('className')).toContain('filled')
    expect(component.find('div.star').at(2).prop('className')).not.toContain('filled')
    expect(component.find('div.star').at(3).prop('className')).not.toContain('filled')
    expect(component.find('div.star').at(4).prop('className')).not.toContain('filled')
  })
})
