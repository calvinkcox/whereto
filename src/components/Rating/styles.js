import styled from 'styled-components'

import colors from '../../theme/colors'

export const RatingLayout = styled.div`
  .star {
  position: relative;
  
  display: inline-block;
  width: 0;
  height: 0;
  
  margin-left: .9em;
  margin-right: .9em;
  margin-bottom: 1.2em;
  
  border-right:  .3em solid transparent;
  border-bottom: .7em  solid ${ colors.linkMutedBlue };
  border-left:   .3em solid transparent;

  font-size: 6px;
  
  &:before, &:after {
    content: '';
    
    display: block;
    width: 0;
    height: 0;
    
    position: absolute;
    top: .6em;
    left: -1em;
  
    border-right:  1em solid transparent;
    border-bottom: .7em  solid ${ colors.linkMutedBlue };
    border-left:   1em solid transparent;
  
    transform: rotate(-35deg);
  }
  
  &.filled {
    border-bottom-color: ${ colors.linkBlue }
  }
  
  &.filled:before, &.filled:after {
    border-bottom-color: ${ colors.linkBlue }
  }
  
  &:after {  
    transform: rotate(35deg);
  }
}
`
