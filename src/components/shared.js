import styled from 'styled-components'

import fonts from '../theme/fonts'
import colors from '../theme/colors'

export const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
`
export const Label = styled.span`
  display: inline-block;
  font-family: ${ fonts.primary };
  padding-bottom: 6px;
  opacity: 0.5;
  font-size: 10px;
  color: #ffffff;
`

export const Avatar = styled.div`
  background: url('images/avatar@3x.png') top left no-repeat;
  background-size: 18px 18px;
  width: 18px;
  height: 18px;
  display: inline-block;
`

export const Button = styled.button`
  & {
    display: inline-block;
    border: none;
    padding: 1rem 2rem;
    margin: 0;
    text-decoration: none;
    background: transparent;
    color: inherit;
    font-family: sans-serif;
    font-size: 1rem;
    cursor: pointer;
    text-align: center;
    transition: background 250ms ease-in-out, transform 150ms ease;
    -webkit-appearance: none;
    -moz-appearance: none;
  }
  
  &:hover,
  &:focus {
      background: transparent;
  }
  
  &:focus {
      outline: 1px solid ${ colors.white };
      outline-offset: -4px;
  }
  
  &:active {
      transform: scale(0.99);
  }  
  
  &.primary {
    background: ${ colors.gradientSecondary };
    color: ${ colors.fiftyPercentWhite };
  }
  
  &.primary:hover,
  &.primary:focus {
    color: ${ colors.white };
  }
`
