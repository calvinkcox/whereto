// @flow
import React from 'react';

import { DetailsLayout, Thumbnail } from './styles'
import type { CarObject } from '../../../models/car-object'

export default function Details(
  {
    onHoverImage,
    isVisible,
    car,
  }:
  {
    onHoverImage: Function,
    isVisible: boolean,
    car: CarObject,
  }) {
  return (
    <DetailsLayout visible={ isVisible }>
      <div>
        <ul>
          <li>Specs</li>
          <li>Reviews</li>
          <li>Parts</li>
        </ul>
        <div>
          Tab 1) Some specs go here!
        </div>
        <div>
          Tab 2) Some reviews go here!
        </div>
        <div>
          Tab 3) Some parts go here!
        </div>
        <div className="images">
          <Thumbnail
            src={ car.image }
            onMouseOver={ () => onHoverImage(true) }
            onMouseOut={ () => onHoverImage(false) }
          />
        </div>
      </div>
    </DetailsLayout>
  )
}
