// @flow
import React from 'react'
import ReactDOM from 'react-dom'
import { shallow } from 'enzyme'
import Details from '../Details'
import { DetailsLayout, Thumbnail } from './styles'

describe('Details', () => {
  let carObj;
  beforeEach(() => {
    carObj = {
      id: 0,
      tags: [],
      title: 'title',
      image: 'image',
      soldBy: {
        name: 'name',
      },
      rating: 1,
    }
  })

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Details car={ carObj } onHoverImage={ () => {} } isVisible />, div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('sets the visible prop on the details', () => {
    const component = shallow(<Details car={ carObj } onHoverImage={ () => {} } isVisible />);
    expect(component.find(DetailsLayout).prop('visible')).toEqual(true)

    component.setProps({
      isVisible: false
    })
    expect(component.find(DetailsLayout).prop('visible')).toEqual(false)
  })

  it('toggles the primary image on when mousing over', () => {
    let args;
    const onHoverImage = (...val) => {
      args = val;
    }
    const component = shallow(<Details car={ carObj } onHoverImage={ onHoverImage } isVisible />);
    component.find(Thumbnail).prop('onMouseOver')()

    expect(args).toEqual([ true ]);
  })

  it('toggles the primary image off when mousing out', () => {
    let args;
    const onHoverImage = (...val) => {
      args = val;
    }
    const component = shallow(<Details car={ carObj } onHoverImage={ onHoverImage } isVisible />);
    component.find(Thumbnail).prop('onMouseOut')()

    expect(args).toEqual([ false ]);
  })
})
