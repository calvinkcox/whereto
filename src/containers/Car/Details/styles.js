// @flow

import styled from 'styled-components'

export const DetailsLayout = styled.div`
  max-height: ${ props => props.visible ? '500px' : '0' };
  transition: max-height 750ms ease-in;
  & > div {
    position: relative;
    margin: 18px 13px;
  } 
`

export const Thumbnail = styled.img`
  width: 150px;
  height: 84px;
`
