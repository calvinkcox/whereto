// @flow
import React from 'react'

import { Row, Avatar } from '../../components/shared'
import { CarLayout, DescriptionColumn, Description, SellsPerson, Action } from './styles'
import HeaderRule from '../../components/HeaderRule'
import Rating from '../../components/Rating'
import Details from './Details'
import type { CarObject } from '../../models/car-object'
import { PrimaryImg } from './styles'

export default class Car extends React.Component<{
  car: CarObject
}, {
  areDetailsVisible: boolean,
  imageHovered: boolean,
}> {

  state = {
    areDetailsVisible: false,
    imageHovered: false,
  }

  setAreDetailsVisible = () => {
    const { areDetailsVisible } = this.state;
    this.setState({
      areDetailsVisible: !areDetailsVisible,
    })
  }

  onHoverImage = (imageHovered: boolean) => {
    this.setState({
      imageHovered
    })
  }

  render () {
    const { setAreDetailsVisible, onHoverImage, props, state } = this;
    const { car } = props;
    const { areDetailsVisible, imageHovered } = state;
    return (
      <CarLayout>
        <PrimaryImg src={ car.image } className={ imageHovered ? 'shown' : '' } />
        <Row>
          <div className="image-container">
            <img src={ car.image } width="147px" height="135px" alt="Picture of the car." />
          </div>
          <DescriptionColumn>
            <Description>
              <h4>{car.title}</h4>
              <p className="description">
                { car.description || 'No description available.' }&nbsp;
                <a className="viewDetails" onClick={ setAreDetailsVisible }>View Details</a>
              </p>
            </Description>
            <SellsPerson>
              <Avatar />
              <div>
                Sold By:&nbsp;
                <a className="soldBy">{ car.soldBy && car.soldBy.name }</a>
              </div>
              <Rating score={ car.rating } />
            </SellsPerson>
          </DescriptionColumn>
          <Action type="button" className="primary">
            Buy
            <HeaderRule />
          </Action>
        </Row>
        <Details onHoverImage={ onHoverImage } car={ car } isVisible={ areDetailsVisible } />
      </CarLayout>
    )
  }
}
