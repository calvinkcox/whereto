import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme'

import Car from '../Car';
import Details from './Details'
import { PrimaryImg } from './styles'

describe('Car', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Car car={ {} } />, div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('shows defaults when there is no description', () => {
    const component = shallow(<Car car={ {} } />);
    const description = component.find('p.description');

    expect(description.text()).toContain('No description available.')
  })

  it('shows description when available', () => {
    const component = shallow(<Car car={ {
      description: 'A description'
    } } />);
    const description = component.find('p.description');

    expect(description.text()).toContain('A description')
  })

  it('shows who sold the vehicle when available', () => {
    const component = shallow(<Car car={ {
      soldBy: null
    } } />);
    expect(component.find('a.soldBy').text()).toEqual('') // precondition

    component.setProps({
      car: {
        soldBy: {
          name: 'name'
        }
      }
    });
    expect(component.find('a.soldBy').text()).toContain('name')
  })

  it('toggles the details section on click of viewDetails', () => {
    const component = shallow(<Car car={ {} } />);
    const button = component.find('a.viewDetails');
    expect(component.find(Details).prop('isVisible')).toEqual(false) // precondition

    button.prop('onClick')()
    expect(component.find(Details).prop('isVisible')).toEqual(true)

    button.prop('onClick')()
    expect(component.find(Details).prop('isVisible')).toEqual(false)
  })

  it('toggles the primary image on and off', () => {
    const component = shallow(<Car car={ {} } />);
    const onHoverImage = component.find(Details).prop('onHoverImage');
    expect(component.find(PrimaryImg).prop('className')).not.toContain('shown') // precondition

    onHoverImage(true)
    expect(component.find(PrimaryImg).prop('className')).toContain('shown')

    onHoverImage(false)
    expect(component.find(PrimaryImg).prop('className')).not.toContain('shown')
  });
})
