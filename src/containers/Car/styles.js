// @flow
import styled from 'styled-components'

import colors from '../../theme/colors'
import { Row, Column, Button } from '../../components/shared'

export const CarLayout = styled(Column)`
  position: relative;
  background-color: ${ colors.white };
  border: 1px solid ${ colors.anotherGray };
  overflow: hidden;
  margin-bottom: 15px;
  
  .image-container {
    width: 147px;
    height: 135px;
  }
`

export const DescriptionColumn = styled(Column)`
  flex: 1;
`

export const Description = styled.div`
  padding: 20px;
  padding-bottom: 0;
  flex: 1;
  
  h4 {
    margin: 0;
  }
  
  p {
    margin: 6px 0;
  }
`

export const SellsPerson = styled(Row)`
  background: ${ colors.yetAnotherGray }
  padding: 0 20px; 
  min-height: 37px;
  align-items: center;
  
  & > div {
    padding-right: 8px;
  }
`

export const Action = styled(Button)`
  min-width: 121px;
`
export const PrimaryImg = styled.img`
  position: absolute;
  z-index: 9;
  top: 18px;
  left: 13px;
  width: 416px;
  height: 234px;
  display: none;
  &.shown {
    display: block;
  }
`
