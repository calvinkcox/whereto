// @flow
import React from 'react'

import { FilterLayout, FilterLabel, CheckboxGroup } from './styles'
import Checkbox from '../../components/Checkbox/index'
import HeaderRule from '../../components/HeaderRule/index'
import DualSlider from '../../components/DualSlider/index'

export default function Filters() {
  return (
    <FilterLayout>
      <FilterLabel>Filters</FilterLabel>
      <HeaderRule width="75px" />
      <CheckboxGroup>
        <Checkbox id="free" label="Free" />
        <Checkbox id="cool" label="Cool" />
        <Checkbox id="fun" label="Fun" />
        <Checkbox id="sexy" label="Sexy" />
      </CheckboxGroup>
      <DualSlider id="dig-it" label="People dig it" />
      <DualSlider id="job-search" label="Helps me with job search" />
    </FilterLayout>
  )
}
