import styled from 'styled-components'

import colors from '../../theme/colors'
import fonts from '../../theme/fonts'
import { Label, Column } from '../../components/shared'

export const FilterLayout = styled.aside`
  min-width: 261px;
  margin-right: 26px;
`

export const FilterLabel = styled(Label)`
  opacity: 1;
  color: ${ colors.black };
  font-size: 12px;
  font-family: ${ fonts.secondary }
`

export const CheckboxGroup = styled(Column)`
  margin-top: 12px;
  margin-bottom: 24px;
`
