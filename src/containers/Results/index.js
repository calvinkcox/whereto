// @flow
import React from 'react';
import { connect } from 'react-redux'

import { ResultsLayout } from './styles'
import Car from '../Car/index'
import type { CarObject } from '../../models/car-object'
import { fetchResults } from '../../redux/results/actions'
import { getCars, isLoadingResults, hasResultsError } from '../../redux/results/selectors'

export class Results extends React.Component<{
  // dispatch props
  fetchResults: Function,
  // state props
  cars: Array<CarObject>,
  isLoading: boolean,
  hasError: boolean,
}> {

  componentDidMount() {
    this.props.fetchResults();
  }

  render() {
    const {
      cars,
      isLoading,
      hasError,
    } = this.props

    if (isLoading) return (<div className="loading">Loading...</div>)

    if (hasError) return (<div className="error">Error Loading Results</div>)

    return (
      <ResultsLayout>
        {
          Array.isArray(cars) && cars.map(car => (
            <Car key={ car.id } car={ car } />
          ))
        }
      </ResultsLayout>
    )
  }
}

const mapStateToProps = state => ({
  cars: getCars(state),
  isLoading: isLoadingResults(state),
  hasError: hasResultsError(state),
})

export default connect(mapStateToProps, { fetchResults })(Results)
