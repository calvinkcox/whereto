// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme'

import { Results } from './index';
import { ResultsLayout } from './styles'
import Car from '../Car'

describe('Results', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <Results
        fetchResults={ ()=> {} }

        cars={ [] }
        isLoading={ false }
        hasError={ false }
      />,
      div);
    ReactDOM.unmountComponentAtNode(div);
  })

  it('shows as loading till there are results', () => {
    const component = shallow(
      <Results
        fetchResults={ () => {} }
        cars={ [] }
        isLoading={ true }
        hasError={ false }
      />
    )
    expect(component.find('div.loading').length).toEqual(1); // precondition
    expect(component.find(ResultsLayout).length).toEqual(0); // precondition

    component.setProps({
      isLoading: false
    })
    expect(component.find('div.loading').length).toEqual(0);
    expect(component.find(ResultsLayout).length).toEqual(1);
  })

  it('shows the error message when a failure occurs', () => {
    const component = shallow(
      <Results
        fetchResults={ () => {} }
        cars={ [] }
        isLoading={ false }
        hasError={ true }
      />
    )
    expect(component.find('div.error').length).toEqual(1); // precondition
    expect(component.find(ResultsLayout).length).toEqual(0); // precondition

    component.setProps({
      hasError: false
    })
    expect(component.find('div.error').length).toEqual(0);
    expect(component.find(ResultsLayout).length).toEqual(1);
  })

  it('adds a car to the layout for every entry in the array', () => {
    const cars = [{
      id: 0,
      tags: [],
      title: 'title',
      image: '',
      soldBy: {
        name: 'name'
      },
      rating: 0
    }, {
      id: 1,
      tags: [],
      title: 'title',
      image: '',
      soldBy: {
        name: 'name'
      },
      rating: 1
    }];
    const component = shallow(
      <Results
        fetchResults={ () => {} }
        cars={ cars }
        isLoading={ false }
        hasError={ false }
      />
    )

    expect(component.find(Car).length).toEqual(2)
    expect(component.find(Car).at(0).prop('car')).toEqual(cars[ 0 ])
    expect(component.find(Car).at(1).prop('car')).toEqual(cars[ 1 ])
  })
})
