// @flow
import React from 'react'

import colors from '../../theme/colors'
import { Label, Row } from '../../components/shared'
import { Actions, Form, Search, SearchAction, Value, ValueGroup } from './styles'
import HeaderRule from '../../components/HeaderRule'

export default function SearchForm() {
  return (
    <Search>
      <Row>
        <Value>
          283 Results for:
          <HeaderRule borderColor={ colors.linkBlue } />
        </Value>
      </Row>
      <Form>
        <ValueGroup>
          <Label>Product</Label>
          <Value>Mercedes Benz</Value>
        </ValueGroup>
        <ValueGroup>
          <Label>Color</Label>
          <Value>Black</Value>
        </ValueGroup>
        <ValueGroup>
          <Label>Year</Label>
          <Value>2009</Value>
        </ValueGroup>
        <ValueGroup>
          <Label>Miles</Label>
          <Value>Less than 1,000</Value>
        </ValueGroup>
        <Actions>
          <SearchAction>Redo Search</SearchAction>
        </Actions>
      </Form>
    </Search>
  )
}
