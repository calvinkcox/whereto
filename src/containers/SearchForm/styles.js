// @flow
import styled from 'styled-components'

import { Row } from '../../components/shared'
import colors from '../../theme/colors'
import fonts from '../../theme/fonts'

export const Form = styled(Row)`
  margin-top: 15px;
`

export const ValueGroup = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 50px;
`

export const Value = styled.strong`
  display: inline-block;
  font-family: ${ fonts.secondary };
  font-size: 16px;
`
export const Search = styled.div`
  color: ${ colors.white };
  padding: 22px 32px;
  background-image: linear-gradient(to right, ${ colors.gradientPrimary }, ${ colors.gradientSecondary });
`
export const SearchAction = styled.a`
  color: ${ colors.linkBlue };
  font-family: ${ fonts.secondary };
  font-size: 16px;
  text-decoration: underline;
`

export const Actions = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`
