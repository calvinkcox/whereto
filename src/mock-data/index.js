export default [ {
  key: 0,
  title: 'Brand New Never Used Mercedes Benz',
  description: 'This is a random description of this car. It\'s only a couple words because this is a test project. Good luck!',
  seller: 'James Raj',
  rating: 2,
  imgSrc: 'images/product-image.jpg',
  details: {
    specs: [ {
      key: 0,
      title: 'Turbo-Engine',
      value: 'This car rips hard'
    }, {
      key: 1,
      title: 'Nitrus',
      value: 'It\'s like Fast and Furious'
    }, {
      key: 2,
      title: '8 cup-holders',
      value: 'For all your friends'
    } ],
    reviews: [ {
      key: 0,
      description: 'review 1'
    }, {
      key: 1,
      description: 'review 2'
    } ]
  }
}, {
  key: 1,
  title: 'Slightly Used Ford Fiesta',
  description: 'This is a random description of this car. It\'s only a couple words because this is a test project. Good luck!',
  seller: 'Calvin Cox',
  rating: 5,
  imgSrc: 'images/product-image.jpg',
  details: {
    specs: [ {
      key: 0,
      title: 'Turbo-Engine',
      value: 'This car rips hard'
    }, {
      key: 1,
      title: 'Nitrus',
      value: 'It\'s like Fast and Furious'
    }, {
      key: 2,
      title: '8 cup-holders',
      value: 'For all your friends'
    } ],
    reviews: [ {
      key: 0,
      description: 'review 1'
    }, {
      key: 1,
      description: 'review 2'
    } ]
  }
}, {
  key: 2,
  title: 'Stolen Civic Type R',
  description: 'This is a random description of this car. It\'s only a couple words because this is a test project. Good luck!',
  seller: 'Elizabeth Perry',
  rating: 4,
  imgSrc: 'images/product-image.jpg',
  details: {
    specs: [ {
      key: 0,
      title: 'Turbo-Engine',
      value: 'This car rips hard'
    }, {
      key: 1,
      title: 'Nitrus',
      value: 'It\'s like Fast and Furious'
    }, {
      key: 2,
      title: '8 cup-holders',
      value: 'For all your friends'
    } ],
    reviews: [ {
      key: 0,
      description: 'review 1'
    }, {
      key: 1,
      description: 'review 2'
    } ]
  }
}, {
  key: 3,
  title: 'Crashed But Still Good GM Silverado',
  description: 'This is a random description of this car. It\'s only a couple words because this is a test project. Good luck!',
  seller: 'Bob Arnold',
  rating: 1,
  imgSrc: 'images/product-image.jpg',
  details: {
    specs: [ {
      key: 0,
      title: 'Turbo-Engine',
      value: 'This car rips hard'
    }, {
      key: 1,
      title: 'Nitrus',
      value: 'It\'s like Fast and Furious'
    }, {
      key: 2,
      title: '8 cup-holders',
      value: 'For all your friends'
    } ],
    reviews: [ {
      key: 0,
      description: 'review 1'
    }, {
      key: 1,
      description: 'review 2'
    } ]
  }
} ]
