// @flow
export interface SoldBy {
  name: string;
}

export interface CarObject {
  id: number;
  tags: Array<string>;
  title: string;
  description?: string;
  image: string;
  soldBy: SoldBy;
  rating: number;
}
