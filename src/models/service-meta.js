export interface Meta {
  isLoading?: boolean;
  errorMessage?: string;
}
