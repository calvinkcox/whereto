// @flow

export type Return<Fn> = $Call<<T>((...Iterable<any>) => T) => T, Fn>;
