import type { CarObject } from '../../models/car-object'
import type { Return } from '../../models/type-extractor'

export const FETCH_RESULTS: string = 'FETCH_RESULTS';
export const FETCH_RESULTS_SUCCESS: string = 'FETCH_RESULTS_SUCCESS';
export const FETCH_RESULTS_ERROR: string = 'FETCH_RESULTS_ERROR';

export const fetchResults = () => ({
  type: FETCH_RESULTS
})

export const fetchResultsSuccess = (results: Array<CarObject>) => ({
  type: FETCH_RESULTS_SUCCESS, payload: results
})

export const fetchResultsError = (errorMessage: string) => ({
  type: FETCH_RESULTS_ERROR, payload: errorMessage
})

export type ResultsActions =
  Return<typeof fetchResults> |
  Return<typeof fetchResultsSuccess> |
  Return<typeof fetchResultsError>
