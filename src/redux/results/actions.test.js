import {
  FETCH_RESULTS,
  FETCH_RESULTS_SUCCESS,
  FETCH_RESULTS_ERROR,
  fetchResults,
  fetchResultsSuccess,
  fetchResultsError,
} from './actions'

describe('Results Actions', () => {
  describe('fetchResults', () => {
    it('dispatches the "FETCH_RESULTS" type', () => {
      expect(fetchResults()).toEqual({
        type: FETCH_RESULTS
      });
    });
  });

  describe('fetchResultsSuccess', () => {
    it('dispatches the "FETCH_RESULTS_SUCCESS" type with the provided payload', () => {
      const payload = {}
      expect(fetchResultsSuccess(payload)).toEqual({
        type: FETCH_RESULTS_SUCCESS,
        payload
      });
    });
  });

  describe('fetchResultsError', () => {
    it('dispatches the "FETCH_RESULTS_ERROR" type with the provided payload', () => {
      const payload = 'error message'
      expect(fetchResultsError(payload)).toEqual({
        type: FETCH_RESULTS_ERROR,
        payload
      });
    });
  });
})
