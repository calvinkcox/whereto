// @flow
import type { CarObject } from '../../models/car-object'
import type { Meta } from '../../models/service-meta'
import type { ResultsActions } from './actions'
import { FETCH_RESULTS_SUCCESS, FETCH_RESULTS_ERROR, FETCH_RESULTS } from './actions'

type State = {
  meta: Meta,
  cars: Array<CarObject>,
}

const defaultState: State = {
  meta: {
    isLoading: false,
    errorMessage: '',
  },
  cars: [],
};

export function resultsReducer(
  state: State = defaultState,
  action: ResultsActions
): State {
  switch (action.type) {
    case FETCH_RESULTS:
      return {
        ...state,
        meta: {
          errorMessage: '',
          isLoading: true,
        }
      }
    case FETCH_RESULTS_SUCCESS:
      return {
        meta: {
          ...state.meta,
          isLoading: false,
        },
        cars: action.payload,
      };

    case FETCH_RESULTS_ERROR:
      return {
        meta: {
          ...state.meta,
          errorMessage: action.payload,
          isLoading: false,
        },
        cars: defaultState.cars,
      };

    default:
      return state;
  }
}
