import { resultsReducer } from './reducer'
import { FETCH_RESULTS, FETCH_RESULTS_ERROR, FETCH_RESULTS_SUCCESS } from './actions'

describe('Results Reducer', () => {
  let initial
  beforeEach(() => {
    initial = {
      meta: {
        isLoading: false,
        errorMessage: '',
      },
      cars: [],
    }
  })

  it('resets errorMessage and sets isLoading to true on FETCH_RESULTS', () => {
    const result = resultsReducer({
      ...initial,
      meta: {
        ...initial.meta,
        errorMessage: 'message'
      }
    }, { type: FETCH_RESULTS });
    expect(result.meta.isLoading).toEqual(true);
    expect(result.meta.errorMessage).toEqual('');
  })

  it('resets isLoading and sets cars on FETCH_RESULTS_SUCCESS', () => {
    const cars = []
    const result = resultsReducer(
      {
        ...initial,
        meta: {
          ...initial.meta,
          isLoading: true,
        }
      },
      { type: FETCH_RESULTS_SUCCESS, payload: cars },
    )
    expect(result.meta.isLoading).toEqual(false);
    expect(result.cars).toEqual(cars);
  })

  it('resets isLoading and sets errorMessage on FETCH_RESULTS_ERROR', () => {
    const result = resultsReducer(
      {
        ...initial,
        meta: {
          ...initial.meta,
          isLoading: true,
        },
        cars: [ 1, 2 ]
      },
      { type: FETCH_RESULTS_ERROR, payload: 'Error Message' },
    )
    expect(result.meta.isLoading).toEqual(false);
    expect(result.meta.errorMessage).toEqual('Error Message');
    expect(result.cars).toEqual([]);
  })
})
