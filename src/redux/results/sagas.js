import { takeLatest, put, call } from 'redux-saga/effects'

import { FETCH_RESULTS, fetchResultsError, fetchResultsSuccess } from './actions'
import apiService from '../../services/api-service'

export const resultsEndpoint = 'https://gist.githubusercontent.com/bgdavidx/f92a8cf0fdceb1a9c3a7e11f4dfec646/raw/ff4946915162d57b8b699ae19d7f5cc1ed317746/car_response.json'

export function* fetchResults() {
  try {
    const { results } = yield call(apiService, resultsEndpoint)
    yield put(fetchResultsSuccess(results))
  } catch (e) {
    yield put(fetchResultsError(e.message))
  }
}

export function* watchFetchResults() {
  yield takeLatest(FETCH_RESULTS, fetchResults)
}
