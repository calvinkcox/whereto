import { call, takeLatest, put } from 'redux-saga/effects'

import { resultsEndpoint, fetchResults, watchFetchResults } from './sagas'
import apiService from '../../services/api-service'
import { FETCH_RESULTS, fetchResultsError, fetchResultsSuccess } from './actions'

describe('Results Sagas', () => {
  describe('watchFetchResults', () => {
    it('calls fetchResults on latest of FETCH_RESULTS action', () => {
      const gen = watchFetchResults()

      expect(gen.next().value).toEqual(
        takeLatest(FETCH_RESULTS, fetchResults)
      )
    })
  })

  describe('fetchResults', () => {
    it('calls the results endpoint via api service and returns success', () => {
      const gen = fetchResults();
      const response = {
        results: {}
      }

      expect(gen.next().value).toEqual(
        call(apiService, resultsEndpoint)
      )
      expect(gen.next(response).value).toEqual(
        put(fetchResultsSuccess(response.results))
      )
      expect(gen.next().done).toEqual(true);
    })

    it('calls the results endpoint via api service and returns an error', () => {
      const gen = fetchResults();

      expect(gen.next().value).toEqual(
        call(apiService, resultsEndpoint)
      )
      expect(gen.throw(new Error('bang!')).value).toEqual(
        put(fetchResultsError('bang!'))
      )
      expect(gen.next().done).toEqual(true);
    })
  })
})
