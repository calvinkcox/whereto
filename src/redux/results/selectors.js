import { createSelector } from 'reselect'

const getResults = state => state.results;

const getResultsMeta = createSelector(
  getResults,
  results => results.meta,
);

export const getCars = createSelector(
  getResults,
  results => results.cars,
)

export const isLoadingResults = createSelector(
  getResultsMeta,
  meta => meta.isLoading,
)

export const hasResultsError = createSelector(
  getResultsMeta,
  meta => !!meta.errorMessage,
)
