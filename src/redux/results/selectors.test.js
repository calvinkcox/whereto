import { getCars, hasResultsError, isLoadingResults } from './selectors'

describe('Results Selectors', () => {
  let initial;
  beforeEach(() => {
    initial = {
      results: {
        meta: {
          isLoading: false,
          errorMessage: ''
        },
        cars: []
      }
    }
  })

  it('allows you to select the cars', () => {
    const cars = [];
    expect(getCars({
      results: {
        ...initial.results,
        cars,
      }
    })).toEqual(cars)
  })

  it('allows you to see if results are loading', () => {
    expect(isLoadingResults({
      results: {
        meta: {
          ...initial.results.meta,
          isLoading: true,
        }
      }
    })).toEqual(true)
  })

  it('allows you to see if results are had an error while loading', () => {
    expect(hasResultsError({
      results: {
        meta: {
          ...initial.results.meta,
          errorMessage: 'message',
        }
      }
    })).toEqual(true)
  })
})
