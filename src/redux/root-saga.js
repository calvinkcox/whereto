import { all } from 'redux-saga/effects'

import { watchFetchResults } from './results/sagas'

export default function* rootSaga() {
  yield all([
    watchFetchResults()
  ])
}
