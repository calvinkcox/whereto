// @flow
export default function apiService<T>(url: string): Promise<T> {
  return fetch(url)
    .then(response => response.json())
}
