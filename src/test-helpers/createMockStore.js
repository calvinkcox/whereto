import { applyMiddleware, compose, createStore } from 'redux';

import rootReducer from '../redux/root-reducer'

export default function createMockStore(initialState) {
  const actions = [];

  const actionsMiddleware = () => next => (action) => {
    actions.push(action);
    return next(action);
  };

  const middleware = [
    actionsMiddleware,
  ];

  const store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware)));

  store.resetActions = () => {
    while (actions.length) {
      actions.pop();
    }
  };

  store.getActions = () => actions.slice(1);

  return store;
}
