export default {
  white: '#fff',
  black: '#000',
  lightGray: '#e7e7e7',
  darkGray: '#e6e6e6',
  anotherGray: '#dddddd',
  yetAnotherGray: '#f9f9f9',
  linkBlue: '#008cdd',
  linkMutedBlue: '#c8e3f3',

  gradientPrimary: '#00658d',
  gradientSecondary: '#003554',

  bodyCopy: '#393939',

  fiftyPercentWhite: 'rgba(255,255,255,0.5)',
}
